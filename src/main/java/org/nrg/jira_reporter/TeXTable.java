package org.nrg.jira_reporter;

import org.apache.commons.lang3.StringUtils;

public class TeXTable {

    private String generatingTeX;

    public TeXTable(String tableStructure) {
        generatingTeX = String.format("\\begin{longtable}%s \\hline ", tableStructure);
    }

    public void addRow(String... elements) {
        generatingTeX += StringUtils.join(elements, " & ") + " \\\\ \\hline ";
    }

    public void endTable() {
        generatingTeX += "\\end{longtable}";
    }

    public String getGeneratingTeX() {
        return generatingTeX;
    }

}
