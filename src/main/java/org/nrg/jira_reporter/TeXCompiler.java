package org.nrg.jira_reporter;

import com.google.common.base.Charsets;
import com.google.common.io.Resources;
import org.apache.commons.io.FileUtils;
import org.apache.commons.io.FilenameUtils;
import org.apache.commons.lang3.StringUtils;
import org.apache.log4j.Logger;
import org.nrg.jira.AssortedUtils;
import org.nrg.jira.JiraManager;
import org.nrg.jira.exceptions.JiraZephyrException;
import org.nrg.jira.testing_components.*;

import java.io.BufferedReader;
import java.io.File;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.net.URL;
import java.nio.file.Files;
import java.nio.file.Path;
import java.text.SimpleDateFormat;
import java.util.*;

public class TeXCompiler {

    private static final String TEX_BASE_NAME = "jira_tex";
    private static final String TEX_FILE_NAME = TEX_BASE_NAME + ".tex";
    private static final String TEX_PDF_NAME  = TEX_BASE_NAME + ".pdf";
    private static final String TEX_RANDOM_NAME = TEX_BASE_NAME + new SimpleDateFormat("yyyyMMdd_HHmmss").format(new Date()) + ".pdf";

    private final List<String> fullDocument = new ArrayList<>();
    private final Logger logger = Logger.getLogger(TeXCompiler.class);
    private final JiraManager jiraManager;
    private final Cycle cycle;
    private final Executions executions;
    private final String jiraUrl;
    private final String project;
    private final String version;
    private final Path tempDirectory;

    public TeXCompiler(JiraReporter jiraReporter, JiraManager jiraManager, Cycle cycle) {
        this.jiraManager = jiraManager;
        this.cycle = cycle;
        executions = cycle.getExecutions();
        jiraUrl = jiraReporter.getUrl();
        project = jiraReporter.getProject();
        version = jiraReporter.getVersion();
        try {
            tempDirectory = Files.createTempDirectory("jirareporter");
        } catch (Exception e) {
            throw new RuntimeException(e);
        }
    }

    public void writeTeX() {
        fullDocument.addAll(getPreamble());
        fullDocument.addAll(getTitlePage());
        fullDocument.addAll(getTableOfContents());
        fullDocument.addAll(getExecutionTeX());
        fullDocument.addAll(getChart());
        fullDocument.addAll(getDisclaimer());
        fullDocument.add("\\end{document}");
        try {
            FileUtils.writeLines(tempDirectory.resolve(TEX_FILE_NAME).toFile(), fullDocument);
        } catch (Exception e) {
            logger.fatal("Error occurred when writing TeX source to file.");
            throw new RuntimeException(e);
        }
    }

    public void compileTeX() {
        pdflatex();
        pdflatex(); // Not redundant, must compile twice for ToC (I think that was it)
        pdflatex(); // Not redundant, must compile third time for page numbers (cries)
    }

    public void copyPDF() {
        try {
            Files.copy(tempDirectory.resolve(TEX_PDF_NAME), new File(TEX_RANDOM_NAME).toPath());
        } catch (Exception e) {
            logger.fatal("Error occurred in copying PDF to current directory.");
            throw new RuntimeException(e);
        }
    }

    private List<String> getPreamble() {
        try {
            final URL preamble = Resources.getResource("preamble.tex");
            return Resources.readLines(preamble, Charsets.UTF_8);
        } catch (Exception e) {
            throw new RuntimeException(e);
        }
    }

    private List<String> getTitlePage() {
        final List<String> titlePage = new ArrayList<>();

        titlePage.add("\\begin{titlepage}");
        titlePage.add("\\centering\\LARGE Test Execution Document for Project: " + project);
        titlePage.add("\\\\");
        titlePage.add("\\vspace{1.5cm}\\Large Test Cycle: " + safeEscape(cycle.getName()));
        titlePage.add("\\\\");
        titlePage.add("\\vspace{1.5cm}\\Large Version: " + safeEscape(version));
        titlePage.add("\\\\");

        if (cycle.getBuild() != null && !cycle.getBuild().isEmpty()) {
            titlePage.add("\\vspace{1cm}\\large Build: " + cycle.getBuild());
            titlePage.add("\\\\");
        }

        if (cycle.getEnvironment() != null && !cycle.getEnvironment().isEmpty()) {
            final String environment = isUrl(cycle.getEnvironment()) ? String.format("\\url{%s}", cycle.getEnvironment()) : cycle.getEnvironment(); // Provide link if environment is a URL
            titlePage.add("\\vspace{1cm}\\large Environment: " + environment);
            titlePage.add("\\\\");
            // TODO: bring back logo functionality
        }

        titlePage.add(String.format("\\vspace*{\\fill}The cycle may be found in JIRA: \\url{%s}.", jiraManager.getProjectLink(project)));
        titlePage.add("\\\\");
        titlePage.add("Compiled \\today{} at \\currenttime.\n\\end{titlepage}\\addtocounter{page}{1}");
        titlePage.add("");
        titlePage.add("");

        return titlePage;
    }

    private List<String> getTableOfContents() {
        final List<String> toc = new ArrayList<>();

        toc.add("{\\Large Non-passing Tests at a Glance:}");
        toc.add("\\begin{enumerate}[label=(\\arabic*)]");
        if (executions.getNumFailing() + executions.getNumBlocked() == 0) {
            toc.add("\t\\item No tests are in 'Failed' or 'Blocked' status. Congratulations!");
        } else {
            if (executions.getNumFailing() > 0) {
                toc.add("\t\\item Failing Test Executions:");
                toc.add("\t\\begin{enumerate}[label=(\\arabic*)]");
                for (Execution execution : executions.getFailingExecutions()) {
                    toc.add(String.format("\t\t\\item \\hyperref[subsec:%s]{%s (%s)}", execution.getIssueKey(), execution.getSummary(), execution.getIssueKey()));
                }
                toc.add("\t\\end{enumerate}");
            }
            if (executions.getNumBlocked() > 0) {
                toc.add("\t\\item Blocked Test Executions:");
                toc.add("\t\\begin{enumerate}[label=(\\arabic*)]");
                for (Execution execution : executions.getBlockedExecutions()) {
                    toc.add(String.format("\t\t\\item \\hyperref[subsec:%s]{%s (%s)}", execution.getIssueKey(), execution.getSummary(), execution.getIssueKey()));
                }
                toc.add("\t\\end{enumerate}");
            }
        }
        toc.add("\\end{enumerate}");
        toc.add("\\newpage");
        toc.add("");
        toc.add("\\tableofcontents");
        toc.add("\\newpage");
        toc.add("");
        toc.add("\\cleardoublepage");
        return toc;
    }

    private List<String> getExecutionTeX() {
        final List<String> exTeX = new ArrayList<>();
        final String tableHeader = "\\begin{longtable}{| L{0.6in} | L{1.3in} | L{1.5in} | L{1in} | L{0.6in} | L{1in} |} \\hline Step Number & Test Step & Test Data & Expected Result & Actual Result & Comment \\\\ \\hline ";
        exTeX.add("\\section{Test Executions}");
        exTeX.add("");
        for (Execution execution : executions.getExecutionList()) {
            final String name           = execution.getSummary();
            final String key            = execution.getIssueKey();
            final String description    = execution.getTestDescription();
            final TestStatus testStatus = execution.getStatus();
            final String executor       = execution.getExecutedByDisplay();
            final String executionTime  = execution.getExecutedOn();
            final String comment        = execution.getComment();

            exTeX.add(String.format("\\subsection{%s (%s)}\\label{subsec:%s}", name, key, key));
            exTeX.add("\\subsubsection{Test Execution Details Summary}");

            final TeXTable summaryTable = new TeXTable("{| L{1.5in} | L{5in} |}");
            summaryTable.addRow("Test name:", name);
            summaryTable.addRow("Test Key:", key);
            if (description != null && !description.isEmpty()) {
                summaryTable.addRow("Description:", safeEscape(description));
            }
            summaryTable.addRow("Execution Status:", colorCode(testStatus));
            summaryTable.addRow("Test Executor", executor);
            summaryTable.addRow("Execution Time: ", executionTime);
            if (comment != null && !comment.isEmpty()) {
                summaryTable.addRow("Comment:", comment);
            }
            summaryTable.endTable();
            exTeX.add(summaryTable.getGeneratingTeX());

            exTeX.add("\\subsubsection{Full Test Execution Step Results}");
            final TeXTable stepResults = new TeXTable("{| L{0.6in} | L{1.3in} | L{1.5in} | L{1in} | L{0.6in} | L{1in} |}");
            stepResults.addRow("Step Number", "Test Step", "Test Data", "Expected Result", "Actual Result", "Comment");
            for (TestStep step : execution.getTestSteps()) {
                final int orderId = step.getOrderId();
                final String testStep = step.getStep();
                final String data = step.getData();
                final String result = step.getResult();
                final TestStatus status = step.getStepStatus();
                final String stepComment = step.getComment();
                int screenshotIndex = 0;

                stepResults.addRow(Integer.toString(orderId), safeEscape(testStep), formatData(data), safeEscape(result), colorCode(status), safeEscape(stepComment));
                if (step.getStepAttachments().size() > 0) {
                    for (Attachment attachment : step.getStepAttachments()) {
                        final String attachmentName = attachment.getFileName();
                        final String extension = FilenameUtils.getExtension(attachmentName);
                        final String extensionLowercase = extension.toLowerCase();
                        if (extensionLowercase.equals("jpg") || extensionLowercase.equals("jpeg") || extensionLowercase.equals("png")) {
                            final String newBaseName = String.format("%s_%d_%d", key, orderId, screenshotIndex);
                            final String newFile = String.format("%s.%s", newBaseName, extension);
                            try {
                                jiraManager.downloadAttachment(attachment, tempDirectory.resolve(newFile).toFile());
                                screenshotIndex++;
                            } catch (JiraZephyrException e) {
                                logger.warn("Could not download attachment from JIRA: " + attachment, e);
                            }
                            stepResults.addRow(String.format("%d (screenshots)", orderId), String.format("\\multicolumn{5}{c|}{\\includegraphics[max width=4 in,valign=T]{%s}}", newFile));
                        }
                    }
                }
            }
            stepResults.endTable();
            exTeX.add(stepResults.getGeneratingTeX());
            exTeX.add("\\newpage");
            exTeX.add("");
            logger.info("Parsed test: " + name);
        }
        return exTeX;
    }

    private List<String> getChart() {
        final int passing = executions.getNumPassing();
        final int failed = executions.getNumFailing();
        final int inProgess = executions.getNumInProgress();
        final int blocked = executions.getNumBlocked();
        final int unexecuted = executions.getNumUnexecuted();
        final List<String> chart = new ArrayList<>();

        chart.add("\\section{Test Execution Results Chart}");
        chart.add("\\centering");
        chart.add("\\begin{tikzpicture}[scale=1.6]");
        chart.add("\t\\begin{axis}[symbolic x coords={Passed, Failed, Blocked, Unexecuted, In Progress}, nodes near coords,xtick={Passed, Failed, Blocked, Unexecuted, In Progress}, ymin=0, xtick pos=left, ytick pos=left, xticklabel style={rotate=45, major tick length=0pt}, title={Test Execution Results}]");
        chart.add(String.format("\t\t\\addplot[ybar,fill=green] coordinates {(Passed, %d)};", passing));
        chart.add(String.format("\t\t\\addplot[ybar,fill=red] coordinates {(Failed, %d)};", failed));
        chart.add(String.format("\t\t\\addplot[ybar,fill=blue] coordinates {(Blocked, %d)};", blocked));
        chart.add(String.format("\t\t\\addplot[ybar,fill=yellow] coordinates {(In Progress, %d)};", inProgess));
        chart.add(String.format("\t\t\\addplot[ybar,fill=gray] coordinates {(Unexecuted, %d)};", unexecuted));
        chart.add("\t\\end{axis}");
        chart.add("\\end{tikzpicture}");
        chart.add("");
        chart.add("");
        return chart;
    }

    private List<String> getDisclaimer() {
        final List<String> disclaimer = new ArrayList<>();
        disclaimer.add("\\vspace*{\\fill}\\small{This PDF was generated and compiled using the JIRA Reporter project, which may be viewed: \\href{https://bitbucket.org/xnatdev/jira-reporter}{here}.}");
        return disclaimer;
    }

    private String colorCode(TestStatus status) {
        switch (status) {
            case PASS:
                return "\\textcolor{OliveGreen}{PASS}";
            case FAIL:
                return "\\textcolor{Red}{FAIL}";
            case UNEXECUTED:
                return "UNEXEC.";
            case BLOCKED:
                return "\\textcolor{MidnightBlue}{BLOCKED}";
            case WIP:
                return "\\textcolor{Yellow}{IN PROGRESS}";
            default:
                return null;
        }
    }

    private String safeEscape(String input) {
        if (input == null) return "";
        input = input.replace("\\", "\\backslash");
        input = input.replace("$", "\\$");
        input = input.replace("\\backslash", "$\\backslash$");
        input = input.replace("#", "\\#");
        input = input.replace("_", "\\_");
        input = input.replace("^", "\\^{}");
        input = input.replace(">", "$>$");
        input = input.replace("<", "$<$");
        input = input.replace("{", "\\{");
        input = input.replace("}", "\\}");
        input = input.replace("&", "\\&");
        input = input.replace("%", "\\%");
        input = input.replace("/", "\\slash ");
        return input;
    }

    private String formatData(String dataString) {
        if (dataString == null || dataString.length() == 0) return "";
        dataString = safeEscape(dataString);
        final String[] dataLines = dataString.split("\n");
        final List<String> readLines = new ArrayList<>();
        for (String dataLine : dataLines) {
            if (dataLine.length() > 30 && maxWordLength(dataLine) > 15 && allCharactersAllowedForSeqsplit(dataLine)) { // use seqsplit package if the string is long, has long components, and only contains characters compatible with the package
                readLines.add(String.format("\\seqsplit{%s}", dataLine.trim()));
            } else {
                readLines.add(dataLine.trim());
            }
        }
        return StringUtils.join(readLines, ", ");
    }

    private boolean allCharactersAllowedForSeqsplit(String input) {
        return input.matches("[A-Za-z0-9\\\\{}._=]+");
    }

    private int maxWordLength(String input) {
        if (input == null || input.isEmpty()) {
            return 0;
        }
        return Collections.max(Arrays.asList(input.split(" ")), new Comparator<String>() {
            @Override
            public int compare(String o1, String o2) {
                return o1.length() - o2.length();
            }
        }).length();
    }

    private boolean isUrl(String input) {
        try {
            new URL(input).toURI();
            return true;
        } catch (Exception e) {
            return false;
        }
    }

    private void pdflatex() {
        try {
            final Process pdflatex;
            final ProcessBuilder processBuilder = new ProcessBuilder("pdflatex", "-halt-on-error", TEX_FILE_NAME);
            processBuilder.directory(tempDirectory.toFile());
            pdflatex = processBuilder.start();

            for (InputStream inputStream : new InputStream[]{pdflatex.getInputStream(), pdflatex.getErrorStream()}) {
                String line;
                BufferedReader bufferedReader = new BufferedReader(new InputStreamReader(inputStream));
                while ((line = bufferedReader.readLine()) != null) {
                    logger.info(line);
                }
            }
        } catch (Exception e) {
            logger.fatal("Error in compiling PDF");
            throw new RuntimeException(e);
        }
    }
}
