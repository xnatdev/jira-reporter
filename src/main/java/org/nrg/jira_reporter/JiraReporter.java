package org.nrg.jira_reporter;

import org.apache.log4j.Logger;
import org.nrg.jira.JiraManager;
import org.nrg.jira.exceptions.*;
import org.nrg.jira.testing_components.Cycle;
import org.nrg.jira.testing_components.Execution;

import java.util.List;

public class JiraReporter {

    private String project, version, cycleName, url, username, password;
    private static final Logger LOGGER = Logger.getLogger(JiraReporter.class);

    public JiraReporter(String url, String username, String password, String project, String version, String cycleName) {
        this.url = url;
        this.username = username;
        this.password = password;
        this.project = project;
        this.version = version;
        this.cycleName = cycleName;
    }

    public void compile() {
        JiraManager jiraManager;
        final Cycle cycle;

        try {
            jiraManager = new JiraManager(url, username, password);
        } catch (JiraNotFoundException e) {
            LOGGER.fatal("JIRA not available at URL: " + url);
            throw new RuntimeException(e);
        } catch (InvalidCredentialsException e) {
            LOGGER.fatal("Credentials provided to JIRA were not valid.");
            throw new RuntimeException(e);
        } catch (ZapiNotFoundException e) {
            LOGGER.fatal("JIRA found at provided URL, but ZAPI does not appear to be installed.");
            throw new RuntimeException(e);
        } catch (JiraZephyrException e) {
            LOGGER.fatal("Unknown error in checking JIRA.");
            throw new RuntimeException(e);
        }

        try {
            cycle = jiraManager.getCycle(cycleName, project, version);
            LOGGER.info("Read cycle...");
        } catch (ProjectNotFoundException e) {
            LOGGER.fatal("Project not found in JIRA: " + project);
            throw new RuntimeException(e);
        } catch (VersionNotFoundException e) {
            LOGGER.fatal(String.format("Fix version %s not found in project %s in JIRA.", version, project));
            throw new RuntimeException(e);
        } catch (CycleNotFoundException e) {
            LOGGER.fatal(String.format("Cycle with name %s not found under fix version %s.", cycleName, version));
            throw new RuntimeException(e);
        } catch (JiraZephyrException e) {
            LOGGER.fatal("Unknown error in getting cycle data from JIRA.");
            throw new RuntimeException(e);
        }

        TeXCompiler compiler = new TeXCompiler(this, jiraManager, cycle);
        compiler.writeTeX();
        compiler.compileTeX();
        compiler.copyPDF();
    }

    public String getProject() {
        return project;
    }

    public String getVersion() {
        return version;
    }

    public String getUrl() {
        return url;
    }

}
